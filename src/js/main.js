let animals = [
    {
        name: 'Black bear',
        weight: '120 - 270 kg',
        speed: '40 km/h',
        img: 'blackbear.jpg'
    },
    {
        name: 'Orca whale',
        weight: '3 600 – 5 400 kg',
        speed: '48 km/h',
        img: 'orca.png'
    },
    {
        name: 'Bald eagle',
        weight: '3 – 6,3 kg',
        speed: '120 – 160 km/h',
        img: 'baldeagle.jpg'
    },
    {
        name: 'Axolotl',
        weight: '3,6 kg',
        speed: '16 km/h',
        img: 'axolotl.jpg'
    }
];

document.addEventListener('DOMContentLoaded', function () {

    function selectAnimal(animal) {
        let img = document.getElementById('selected-animal-img');
        let name = document.getElementById('selected-animal-name');
        let weight = document.getElementById('selected-animal-weight');
        let speed = document.getElementById('selected-animal-speed');

        img.setAttribute('src', 'img/' + animal.img);
        img.setAttribute('alt', animal.name);
        name.innerText = animal.name;
        weight.innerText = animal.weight;
        speed.innerText = animal.speed;
    }

    function addAnimal(animal) {
        let container = document.getElementById('animals');

        let item = document.createElement('li');
        let img = document.createElement('img');
        let name = document.createElement('p');

        img.setAttribute('src', 'img/' + animal.img);
        img.setAttribute('alt', animal.name);
        img.setAttribute('height', '100');
        name.innerText = animal.name;

        item.appendChild(img);
        item.appendChild(name);
        item.addEventListener('click', function () {
            selectAnimal(animal);
        });

        container.appendChild(item);
    }

    function addAnimals() {
        animals.forEach(addAnimal);
    }

    addAnimals();
    selectAnimal(animals[0]);
});

